package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4484146589817098769L;
	
	public void init() throws ServletException{
		System.out.println("*****************************************");
		System.out.println("DetailsServlet has been initialized");
		System.out.println("*****************************************");
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		ServletContext srvContext = getServletContext();
		String branding = srvContext.getInitParameter("branding");
		//First name System Properties
		String firstName = System.getProperty("fName");
		//Last name Http Session
		HttpSession session = req.getSession();
		String lastName = (String)session.getAttribute("lName");
		//Contact sendRedirect method
		String contact = System.getProperty("contact");
		//Email setAttribute method
		String email = (String) srvContext.getAttribute("email");
		
		PrintWriter out = res.getWriter();
		out.println("<h1>Welcome to Phonebook</h1>" + 
		"<p>Branding: " +branding+ "</p>" +
		"<p>First Name: " +firstName+ "</p>" +
		"<p>Last Name: " +lastName+ "</p>" +
		"<p>Contact: " +contact+ "</p>" +
		"<p>Email: " +email+ "</p>");
	}
	public void destroy() {
		System.out.println("*****************************************");
		System.out.println("DetailsServlet has been destroyed");
		System.out.println("*****************************************");
	}
}
