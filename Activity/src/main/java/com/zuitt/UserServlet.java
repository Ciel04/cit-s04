package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {



	/**
	 * 
	 */
	private static final long serialVersionUID = -7939269326270043021L;
	
	public void init() throws ServletException{
		System.out.println("*****************************************");
		System.out.println("UserServlet has been initialized");
		System.out.println("*****************************************");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		//Branding Servlet Context
		ServletContext srvContext = getServletContext();
		String branding = srvContext.getInitParameter("branding");
		
		//First name System Properties
		String firstName = req.getParameter("firstname");
		System.getProperties().put("fName", firstName);
		String firstNam = System.getProperty("fName");
		
		//Last name HttpSession
		String lastName = req.getParameter("lastname");
		HttpSession session = req.getSession();
		session.setAttribute("lName", lastName);
		String lastNam = (String) session.getAttribute("lName");
		
		//Email srvContext setAttribute method
		String email = req.getParameter("email");
		srvContext.setAttribute("email", email);
		
		//Contact URL rewriting via sendRedirect method
		String contact = req.getParameter("contact");
		System.getProperties().put("contact", contact);
		res.sendRedirect("details?contact="+contact);
		
		PrintWriter out = res.getWriter();
		out.println("<h1>Welcome to Phonebook</h1>" + 
		"<p>Branding: " +branding+ "</p>" +
		"<p>First Name: " +firstNam+ "</p>" +
		"<p>Last Name: " +lastNam+ "</p>" +
		"<p>Contact: " +contact+ "</p>" +
		"<p>Email: " +email+ "</p>");
	}
	public void destroy() {
		System.out.println("*****************************************");
		System.out.println("UserServlet has been destroyed");
		System.out.println("*****************************************");
	}

}
